var gulp = require('gulp');
var sass = require('gulp-sass');
var cssnano = require('gulp-cssnano');
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var del = require('del');
var runSequence = require('run-sequence');
var browserSync = require('browser-sync').create();
var cache = require('gulp-cache');
var imagemin = require('gulp-imagemin');

gulp.task('del', function () {
	del.sync('dist/*');
});

gulp.task('copy', function () {
	gulp.src('src/*.php')
		.pipe(gulp.dest('dist'));
	gulp.src('src/parts/*.php')
		.pipe(gulp.dest('dist/parts'));
	gulp.src('src/css/fonts/*.{eot, tff, woff, woff2}') // or "src/css/fonts/*" for all files
		.pipe(gulp.dest('dist/css/font'));
	gulp.src(['src/js/owl.carousel.css', 'src/js/owl.carousel.min.js'])
		.pipe(gulp.dest('dist/js'));
});

gulp.task('sass', function () {
	gulp.src('src/scss/**/*.scss')
		.pipe(sass())
		.pipe(gulp.dest('src/css'));
});

gulp.task('cssnano', function () {
	gulp.src('src/css/*.css')
		.pipe(cssnano())
		.pipe(gulp.dest('dist/css'));
});

gulp.task('jshint', function () {
	gulp.src('src/js/custom.js')
		.pipe(jshint())
		.pipe(jshint.reporter('default'))
});

gulp.task('compress-js', function () {
	gulp.src('src/js/custom.js')
		.pipe(jshint())
		.pipe(jshint.reporter('default'))
		.pipe(uglify())
		.pipe(gulp.dest('dist/js'));
});

gulp.task('browserSync', function () {
	browserSync.init({
		proxy: 'http://localhost:8080/w442',
	});
});

gulp.task('watch', ['browserSync', 'sass', 'jshint'], function () {
	gulp.watch('src/scss/**/*.scss', function (callback) {
		runSequence('sass', browserSync.reload());
	});
	gulp.watch('src/js/custom.js', function (callback) {
		runSequence('jshint', browserSync.reload());
	});
	gulp.watch('src/**/*.php', browserSync.reload());
});

gulp.task('imagemin', function () {
	gulp.src('src/images/*')
		.pipe(cache(imagemin()))
		.pipe(gulp.dest('dist/images'))
});

gulp.task('default', function (callback) {
	runSequence(['sass', 'jshint' ,'browserSync', 'watch'], callback);
});

gulp.task('build', function (callback) {
	runSequence('del', ['copy', 'cssnano', 'compress-js', 'imagemin'], callback);
});