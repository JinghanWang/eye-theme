<?php 

if (get_query_var('post_type') == 'frame') {
	$GLOBALS['frame_properties'] = [];

	function get_frames_search_query($tax) {
		$tags = get_query_var($tax);
		get_frames_term_name($tax, $tags);
	}

	function get_frames_term_name($tax, $taxs) {
		if (!!($taxs)) {
			foreach ($taxs as $tag) {
				$tag_name = get_term_by('slug', $tag, $tax)->name;
				array_push($GLOBALS['frame_properties'], $tag_name);
			}
		}
	}

	function get_frame_search_order($orderby, $order) {
		$search = $_SERVER["REQUEST_URI"];
		$order_string_1 = '&orderby=suggested_retail_price&order=ASC';
		$order_string_2 = '&orderby=suggested_retail_price&order=DESC';
		$order_string_3 = '&orderby=title&order=ASC';
		$order_string_4 = '&orderby=title&order=DESC';
		if (strpos($search, $order_string_1) !== false) {
			$search = str_replace($order_string_1, '', $search);
		}
		if (strpos($search, $order_string_2) !== false) {
			$search = str_replace($order_string_2, '', $search);
		}
		if (strpos($search, $order_string_3) !== false) {
			$search = str_replace($order_string_3, '', $search);
		}
		if (strpos($search, $order_string_4) !== false) {
			$search = str_replace($order_string_4, '', $search);
		}
		$search = $search . '&orderby=' . $orderby . '&order=' . $order;
		echo $search;
	}

	$trends = get_query_var('trends');
	if (!empty($trends)) {
		$trends = $trends[0];
		$trends = explode("," , $trends);
		get_frames_term_name('trends', $trends);
	}
	get_frames_search_query('gender');
	get_frames_search_query('product');
	get_frames_search_query('suggested_retail_price');
	get_frames_search_query('frame_material');
	get_frames_search_query('color');
	get_frames_search_query('details');
	get_frames_search_query('style');
	get_frames_search_query('face_shape');
}

get_header(); 
?>
			
	<div id="content">

		<div class="row">
			<div class="large-12 medium-12 columns">
				<div class="banner-image">
					<?php echo do_shortcode('[contentblock id=10]'); ?>
				</div>
			</div>
		</div>

		<div id="inner-content" class="row">

	
			<main id="main" class="large-9 medium-8 columns first" role="main">

				<header>
					<h1 class="archive-title">Search Results for:
						<?php if (get_query_var('post_type') == 'frame') { ?>
							<span><?php echo implode(", ", $GLOBALS['frame_properties']); ?></span>
						<?php } else {
							echo esc_attr(get_search_query());
						} ?>
					</h1>
					<?php if (get_query_var('post_type') == 'frame') { 
						$orderby = $GLOBALS['orderby'];
						$order = $GLOBALS['order'];
						if ($orderby === 'title' && $order === 'ASC') {
							$sortby = 'Title A-Z';
						} else if ($orderby === 'title' && $order === 'DESC') {
							$sortby = 'Title Z-A';
						} else if ($orderby === 'meta_value_num' && $order === 'ASC') {
							$sortby = 'Price Low-High';
						} else if ($orderby === 'meta_value_
							num' && $order === 'DESC') {
							$sortby = 'Price High-Low';
						}
						?>
						<h3>Sort these results by: <?php echo $sortby; ?></h3>
						<ul class="sort-results">
							<li><a href="<?php get_frame_search_order('suggested_retail_price', 'ASC'); ?>">Price Low-High</a></li>
							<li><a href="<?php get_frame_search_order('suggested_retail_price', 'DESC'); ?>">Price High-Low</a></li>
							<li><a href="<?php get_frame_search_order('title', 'ASC'); ?>">Title A-Z</a></li>
							<li><a href="<?php get_frame_search_order('title', 'DESC'); ?>">Title Z-A</a></li>
						</ul>
					<?php } ?>
				</header>

				<div class="search-result">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<?php 
						$post_type = get_query_var('post_type');

						if ($post_type == 'frame') {
							echo '<div class="search-frames">';
							get_template_part( 'parts/loop', 'frame' );
							echo '</div>';
						} else {
							echo '<div class="search-site">';
							get_template_part( 'parts/loop', 'tax' );
							echo '</div>';
						}
						?>
					<?php endwhile; ?>
					<div class="clearfix"></div>
				</div>

				<?php joints_page_navi(); ?>
					
				<?php else : ?>
				
				<?php get_template_part( 'parts/content', 'missing' ); ?>
						
			    <?php endif; ?>
	
		    </main> <!-- end #main -->
		
		    <?php get_sidebar(); ?>
		
		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>
