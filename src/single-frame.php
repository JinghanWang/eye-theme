<?php
/*
This is the custom post type post template.
If you edit the post type name, you've got
to change the name of this template to
reflect that name change.

i.e. if your custom post type is called
register_post_type( 'bookmarks',
then your single template should be
single-bookmarks.php

*/
?>

<?php 



get_header(); ?>
			
<div id="content">

	<div class="row">
		<div class="large-12 medium-12 columns">
			<div class="banner-image">
				<?php 
				if (have_posts()) : while (have_posts()) : the_post(); 
				$gender = wp_get_post_terms( $post->ID, 'gender', array("fields" => "names") );
				if ($gender[0] == 'Male') {
					echo '<div class="banner-image">' . do_shortcode('[contentblock id=11]') . '</div>';
				} else {
					echo '<div class="banner-image">' . do_shortcode('[contentblock id=12]') . '</div>';
				}
				?>	
			</div>
		</div>
	</div>

	<div id="inner-content" class="row">

		<main id="main" class="large-9 medium-8 columns first frame" role="main">
		
    	<div class="breadcrumbs" typeof="BreadcrumbList">
		    <?php if(function_exists('bcn_display'))
		    {
		        bcn_display();
		    }?>
			</div>

	    <article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
					
				<header class="article-header">	
					<h1><?php the_title(); ?> (<?php the_field('frame_name_or_style'); ?>)<h1>
					<p><a href="javascript:history.go(-1)">GO BACK</a></p>
			    </header> <!-- end article header -->
								
			  <section class="entry-content" itemprop="articleBody">
					<div class="single-frame-full">
						<div class="frame-images">
							<?php the_post_thumbnail(array(600, 300)); ?>
							<?php $hi_res_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>
							<?php $low_res_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' ); ?>
							<a href="<?php echo $hi_res_image[0]; ?>" class="hi-res" target="_blank">HI-RES IMAGE</a>
							<a href="<?php echo $low_res_image[0]; ?>" class="low-res" target="_blank">LOW-RES IMAGE</a>
						</div>
						<div class="frame-properites">
							<div class="frame-title"><?php the_title(); ?></div>
							<div class="frame-name-or-style">Style: <?php the_field('frame_name_or_style'); ?></div>
							<?php get_single_tax_terms($post, 'trends'); ?>
							<?php get_single_tax_terms_without_name($post, 'product'); ?>
							<?php get_single_tax_terms_without_name($post, 'gender'); ?>
							<?php get_single_tax_terms($post, 'suggested_retail_price'); ?>
							<?php get_single_tax_terms($post, 'frame_material'); ?>
							<?php get_single_tax_terms($post, 'color'); ?>
							<?php get_single_tax_terms($post, 'details'); ?>
							<?php get_single_tax_terms($post, 'style'); ?>
							<?php get_single_tax_terms($post, 'face_shape'); ?>
							<?php get_single_tax_terms($post, 'age'); ?>
							<?php // the_content(); ?>
						</div>
					</div>
				</section> <!-- end article section -->
									
				<footer class="article-footer">
					<p class="tags"><?php the_tags('<span class="tags-title">' . __( 'Tags:', 'jointswp' ) . '</span> ', ', ', ''); ?></p>	
				</footer> <!-- end article footer -->
																
			</article> <!-- end article -->
		    					
		<?php endwhile; else : ?>

		<main id="main" class="large-9 medium-8 columns first frame" role="main">
		
	   		<?php get_template_part( 'parts/content', 'missing' ); ?>

	    <?php endif; ?>

		</main> <!-- end #main -->

		<?php get_sidebar(); ?>

	</div> <!-- end #inner-content -->

</div> <!-- end #content -->

<?php get_footer(); ?>