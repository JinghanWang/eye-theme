<?php 
/**
 * Template Name: Custom Search
 * This is the template that displays programs filter pages.
 *
 */
$taxonomy = '';

function build_checkbox ($taxonomy) {
	$tags = get_terms($taxonomy);
	$taxonomy_name = get_taxonomy($taxonomy)->labels->name;
	$checkboxes = '<div class="search-property ' . $taxonomy . '"><div class="search-checkbox-label">' . $taxonomy_name . '</div><div class="search-checkbox-container">' ;
	foreach($tags as $tag) :
		$checkboxes .='<input type="checkbox" name="' . $taxonomy . '[]" value="' . $tag->slug . '" id="tag-' . $tag->term_id . '" /><label for="tag-' . $tag->term_id . '"><span></span>' . $tag->name . '</label><br>';
	endforeach;
	$checkboxes .= '</div></div>';
	print $checkboxes;
}
get_header(); ?>
	
	<div id="content">

		<div class="row">
			<div class="large-12 medium-12 columns">
				<div class="banner-image">
					<?php get_banner_image('header_banner_image', 'header_banner_image_mobile'); ?>	
				</div>
			</div>
		</div>

		<div id="inner-content" class="row">

		    <main id="main" class="large-9 medium-8 columns" role="main">
				
		    	<div class="page-content">

		    		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<div class="breadcrumbs" typeof="BreadcrumbList">
					    <?php if(function_exists('bcn_display'))
					    {
					        bcn_display();
					    }?>
					</div>

					<div id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
											
						<header class="article-header">
							<h1 class="page-title"><?php the_title(); ?></h1>
						</header> <!-- end article header -->
										
					    <section class="entry-content" itemprop="articleBody">
						    <?php the_content(); ?>
						    <?php wp_link_pages(); ?>
						</section> <!-- end article section -->
										
					</div> <!-- end article -->

					<?php endwhile; endif; ?>

				</div>

				<div class="frame-search">

					<form role="search" method="get" id="searchform" action="<?php bloginfo('url'); ?>">
						<div>
							<?php 
								$tags = get_terms('trends');
								$trends = '';
								foreach ($tags as $tag) {
									$trends .= $tag->slug . ',';
								}
								$trends = substr($trends, 0, strlen($trends)-1);
							?>
							<div class="search-property trends">
								<div class="search-checkbox-container">
									<input type="checkbox" name="trends[]" value="<?php echo $trends; ?>" id="trends"/><label for="trends"><span></span>Only Search Frames from Featured Trends</label>
								</div>
							</div>
							<input type="hidden" value="" name="s" id="s">
							<input type="hidden" value="1" name="sentence">
							<input type="hidden" value="frame" name="post_type">
							<?php build_checkbox('gender'); ?>
							<?php build_checkbox('product'); ?>
							<?php build_checkbox('suggested_retail_price'); ?>
							<?php build_checkbox('frame_material'); ?>							
							<?php build_checkbox('color'); ?>
							<?php build_checkbox('details'); ?>
							<?php build_checkbox('style'); ?>
							<?php build_checkbox('face_shape'); ?>
							<input type="submit" id="searchsubmit" value="Search eyewear" />
						</div>
					</form>

				</div>
			    					
			</main> <!-- end #main -->

		    <?php get_sidebar(); ?>
		    
		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>