<?php
/**
*	Enqueue SASS compiled CSS file
*	Enqueue owl carousel slider
*/
function child_theme_style() {
  wp_enqueue_style( 'child-theme-style', get_stylesheet_directory_uri() . '/css/style.css', '', '1.0');
  wp_enqueue_style( 'owl-slider-css', get_stylesheet_directory_uri() . '/js/owl.carousel.css', '', '1.0');
  wp_enqueue_script( 'owl-slider-js', get_stylesheet_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), '1.0.0', false );
  wp_enqueue_script( 'child-custom-js', get_stylesheet_directory_uri() . '/js/custom.js', array('jquery'), '1.0.0', false );
}
add_action('wp_enqueue_scripts', 'child_theme_style', 1000);

/**
*	List Terms of Frame content type
*/
function get_frame_terms ($post) {
	$taxonomy = get_object_taxonomies($post);
	$terms = '';
	$args = array("fields" => "names");
	foreach ($taxonomy as $tax) {
		$terms = wp_get_post_terms( $post->ID, $tax, $args );
		$string = '';
		foreach ($terms as $value) {
			$string .= ",$value";
		}
		$string = substr($string, 1);
		print '<div class="' . $tax . '">' . $string . '</div>';
	}
}

/**
*	Get Terms of a Taxonomy
*/
function get_single_tax_terms ($post, $tax) {
	$args = array("fields" => "names");
	$terms = wp_get_post_terms( $post->ID, $tax, $args );
	$tax_name = get_taxonomy($tax)->labels->name;
	$string = '';
	foreach ($terms as $value) {
		$string .= ",$value";
	}
	$string = substr($string, 1);
	print '<div class="' . $tax . '">' . $tax_name . ": " . $string . '</div>';
}

/**
*	Get Terms of a Taxonomy without Taxonomy name
*/
function get_single_tax_terms_without_name ($post, $tax) {
	$args = array("fields" => "names");
	$terms = wp_get_post_terms( $post->ID, $tax, $args );
	$string = '';
	foreach ($terms as $value) {
		$string .= ",$value";
	}
	$string = substr($string, 1);
	print '<div class="' . $tax . '">' . $string . '</div>';
}

/**
*	Feature Slider Function
*/
function get_feature_slider ($cat) {
	$slider_cat = $cat;
	$args = array(
			'post_type' => 'feature_slides',
			'ignore_sticky_posts' => 1, 
			'posts_per_page' => -1,
			'orderby' => 'title',
			'order'   => 'ASC',
			'tax_query' => array(
					array(
						'taxonomy' => 'slide_cat',
						'field' => 'slug',
						'terms' => $slider_cat,
					),
				),
		);
	$slides = new WP_Query($args);
	print ('<div class="feature-slider">');
	while ($slides->have_posts()) : $slides->the_post();

		print ('<a href="');
		the_field('slider_link');
		print ('"><div class="feature-slide"><div class="slide-image">');
		the_post_thumbnail('full');
		print ('</div><div class="slide-caption"><div class="slide-tilte">');
		the_title();
		print ('</div><div class="slide-content">');
		the_content();
		print ('</div></div></div></a>');

	endwhile; wp_reset_postdata();
	print ('</div>');
}

/**
*	Order by Custom taxonomy
*/
function custom_orderby( $query ) {  
	if (array_key_exists('orderby', $_GET)) {
		if( $_GET['orderby'] == 'suggested_retail_price' ){
	        set_query_var('orderby', 'meta_value_num');
	        set_query_var('meta_key', $_GET['orderby']);
	        set_query_var('order', $_GET['order']);
	    } else {
	    	set_query_var('orderby', $_GET['orderby']);
	    	set_query_var('order', $_GET['order']);
	    }
	}
}
add_action( 'pre_get_posts', 'custom_orderby' ); 

/**
*	Remove width in caption shortcode
*/
add_shortcode('wp_caption', 'fixed_img_caption_shortcode');
add_shortcode('caption', 'fixed_img_caption_shortcode');
function fixed_img_caption_shortcode($attr, $content = null) {
	if ( ! isset( $attr['caption'] ) ) {
		if ( preg_match( '#((?:<a [^>]+>\s*)?<img [^>]+>(?:\s*</a>)?)(.*)#is', $content, $matches ) ) {
			$content = $matches[1];
			$attr['caption'] = trim( $matches[2] );
		}
	}
	$output = apply_filters('img_caption_shortcode', '', $attr, $content);
	if ( $output != '' )
		return $output;
	extract(shortcode_atts(array(
		'id'	=> '',
		'align'	=> 'alignnone',
		'width'	=> '',
		'caption' => ''
	), $attr));
	if ( 1 > (int) $width || empty($caption) )
		return $content;
	if ( $id ) $id = 'id="' . esc_attr($id) . '" ';
	return '<div ' . $id . 'class="wp-caption ' . esc_attr($align) . '" >' . do_shortcode( $content ) . '<p class="wp-caption-text">' . $caption . '</p></div>';
}

/**
*	Call different banner eimages for desktop and mobile
*/
function get_banner_image ($big_image, $small_image) {
	$image = get_field($big_image);
	$image_mobile = get_field($small_image);
	if (empty($image_mobile)) {
		$image_mobile = $image;
	}
	if (!empty($image)) { 
		$large = wp_get_attachment_image_src($image, 'full');
		$medium = wp_get_attachment_image_src($image_mobile, 'full');
		echo '<img data-interchange="[' . $medium[0] . ', small], [' . $large[0] . ', medium], [' . $large[0] . ', large]">';
	} 
}

/**
*	Change Read More text
*/
add_filter( 'excerpt_more', 'modify_read_more_link', 100 );
function modify_read_more_link() {
	return '<span>... <span><a class="excerpt-read-more" href="' . get_permalink() . '">Read more &raquo;</a>';
}