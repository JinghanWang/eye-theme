<?php 
/**
*	Template Name: Blog List Page
*/

get_header(); ?>
			
	<div id="content">
	
		<div id="inner-content" class="row">
	
		    <?php get_feature_slider('blog-list'); ?>

		    <main id="main" class="large-9 medium-8 columns" role="main">
		    
			    <?php 

			    the_title('<h1>', '</h1>');

			    global $wp_query;

				$args = array(
						'post_type' => 'post',
						'posts_per_page' => 10,
						'paged' => $paged,
					);
				$wp_query = new WP_Query($args);

			    if ($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
			 
					
						<?php get_template_part( 'parts/loop', 'blog' ); ?>
				
				    
				<?php endwhile; ?>	

					<?php joints_page_navi(); ?>
					
				<?php else : ?>
											
					<?php get_template_part( 'parts/content', 'missing' ); ?>
						
				<?php endif; ?>

				<?php wp_reset_query(); ?>
																								
		    </main> <!-- end #main -->
		    
		    <?php get_sidebar(); ?>

		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer();

?>