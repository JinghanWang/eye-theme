<?php 

/**
 * Template Name: Home Page
 * This is the template for home page.
 *
 */

get_header(); ?>
	
	<div id="content">
		<div class="row">
			<div class="large-12 medium-12 columns">
				<div class="banner-image">
					<?php get_feature_slider('home-page'); ?>
				</div>
			</div>
		</div>

		<div class="row mission-statement">	
			<div class="large-8 medium-10 columns">
				<?php the_field('text_under_slider'); ?>
			</div>
		</div>

		<div id="inner-content">
			<div class="row">
			     <main id="main" class="large-9 medium-8 columns" role="main">
					
					<div id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
										
					    <section class="entry-content" itemprop="articleBody">
						    <?php the_content(); ?>
						</section> <!-- end article section -->
										
					</div> <!-- end article -->

				 	<div class="home-trend">
				 		<a href="<?php the_field('trend_1_link'); ?>">
				 			<div class="trend-overlay">
					 			<?php $image = get_field('trend_1_image'); ?>
					 			<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					 			<?php the_field('trend_1_description'); ?>
					 		</div>
				 		</a>
				 		<a href="<?php the_field('trend_1_link'); ?>"><h3><span><?php the_field('trend_1_name'); ?></span></h3></a>
				 	</div>
				 	<div class="home-trend">
				 		<a href="<?php the_field('trend_2_link'); ?>">
					 		<div class="trend-overlay">
					 			<?php $image = get_field('trend_2_image'); ?>
					 			<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					 			<?php the_field('trend_2_description'); ?>
					 		</div>
					 	</a>
				 		<a href="<?php the_field('trend_2_link'); ?>"><h3><span><?php the_field('trend_2_name'); ?></span></h3></a>
				 	</div>
				 	<div class="home-trend">
				 		<a href="<?php the_field('trend_3_link'); ?>">
					 		<div class="trend-overlay">
					 			<?php $image = get_field('trend_3_image'); ?>
					 			<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					 			<?php the_field('trend_3_description'); ?>
					 		</div>
					 	</a>
				 		<a href="<?php the_field('trend_3_link'); ?>"><h3><span><?php the_field('trend_3_name'); ?></span></h3></a>
				 	</div>
				 	<div class="home-trend">
				 		<a href="<?php the_field('trend_4_link'); ?>">
					 		<div class="trend-overlay">
					 			<?php $image = get_field('trend_4_image'); ?>
					 			<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					 			<?php the_field('trend_4_description'); ?>
					 		</div>
					 	</a>
				 		<a href="<?php the_field('trend_4_link'); ?>"><h3><span><?php the_field('trend_4_name'); ?></span></h3></a>
				 	</div>					
				    					
				</main> <!-- end #main -->

			    <?php get_sidebar(); ?>
			</div>  <!--end main row--> 
		</div> <!-- end #inner-content -->
	</div> <!-- end #content -->

<?php get_footer(); ?>