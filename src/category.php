<?php get_header(); ?>
			
	<div id="content">
	
		<div class="row">
			<div class="large-12 medium 12 columns">
				<div class="banner-image">
					<?php echo do_shortcode('[contentblock id=10]'); ?>
				</div>
			</div>
		</div>

		<div id="inner-content" class="row">

			<h1 class="page-title">Posts in category: <?php single_cat_title(); ?></h1>
	
		    <main id="main" class="large-9 medium-8 columns" role="main">
		    
			    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			 
					<!-- To see additional archive styles, visit the /parts directory -->
					<div class="blog-list-post-container"><?php get_template_part( 'parts/loop', 'blog' ); ?></div>
				    
				<?php endwhile; ?>	

					<?php joints_page_navi(); ?>
					
				<?php else : ?>
											
					<?php get_template_part( 'parts/content', 'missing' ); ?>
						
				<?php endif; ?>
																								
		    </main> <!-- end #main -->
		    
		    <?php get_sidebar(); ?>

		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>