<?php 
/**
*	Template Name: Single Trend Page
*/

get_header(); ?>

	<div id="content">

		<div class="row">
			<div class="large-12 medium-12 columns">
				<div class="banner-image">
					<?php get_banner_image('header_banner_image', 'header_banner_image_mobile'); ?>	
				</div>
			</div>
		</div>

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	
		<div id="inner-content" class="row">
	
		    <main id="main" class="large-9 medium-8 columns" role="main">

		    	<div class="breadcrumbs" typeof="BreadcrumbList">
				    <?php if(function_exists('bcn_display'))
				    {
				        bcn_display();
				    }?>
				</div>

				<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">										
					<header class="article-header">
						<h1 class="page-title"><?php the_title(); ?></h1>
					</header> <!-- end article header -->
									
				    <section class="entry-content" itemprop="articleBody">
					    <?php the_content(); ?>
					    <?php wp_link_pages(); ?>
					</section> <!-- end article section -->
										
					<footer class="article-footer">
					    <div class="gender-frames-container">
						<div class="women-frames">
							<h3 class="frame-title"><a href="<?php the_field('womens_link'); ?>">WOMEN'S FRAMES</a></h3>
							<div class="frame-image">
								<a href="<?php the_field('womens_link'); ?>"><img src="<?php the_field('womens_frames'); ?>"></a>
							</div>
						</div>
						<div class="men-frames">
							<h3 class="frame-title"><a href="<?php the_field('mens_link'); ?>">MEN'S FRAMES</a></h3>
							<div class="frame-image">
								<a href="<?php the_field('mens_link'); ?>"><img src="<?php the_field('mens_frames'); ?>"></a>
							</div>
						</div>
						</div>
					</footer> <!-- end article footer -->				
				</article> <!-- end article -->
			    
			<?php endwhile; endif; ?>							
			    					
			</main> <!-- end #main -->

		    <?php get_sidebar(); ?>
		    
		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>