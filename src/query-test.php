<?php 
/**
 * Template Name: Query Test
 * This is the template that displays programs filter pages.
 *
 */

get_header(); ?>
	
	<div id="content">
<div id="inner-content" class="row">
	
		    <main id="main" class="large-8 medium-8 columns" role="main">
		<?php 
			$args = array(
						'post_type' => 'frame',
						// 'meta_query' => array(
						// 	'suggested_retail_price' => array(
						// 			'key' => 'suggested_retail_price',
						// 		)
						// 	),
						'meta_key' => 'suggested_retail_price',
    					'orderby' => 'meta_value_num',
						'order' => 'ASC'
					);
			$test_query = new WP_Query( $args ); 
		?>

		<?php while ($test_query->have_posts()) : $test_query->the_post(); ?>
	
		

			    	<?php get_template_part( 'parts/loop', 'frame' ); ?>
			    
			    <?php endwhile; wp_reset_query();?>							
			    					
			</main> <!-- end #main -->

		    <?php get_sidebar(); ?>
		    
		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>