jQuery(document).ready(function($) {
	
	// Feature Slider

	var owl = $(".feature-slider");

	owl.owlCarousel({
		nav: true,
		center: true,
		loop: true,
		margin: 30,
		autoplay: true,
		autoplayTimeout: 4000,
		autoplayHoverPause: true,
		responsive: {
			0: {
				items: 1,
			},
			800: {
				items: 2,
			},
		},
	});

	// $(window).load(function(){
	// 	if ($(window).width() > 640) {
	// 		$('.page-template-default #main').height($('.page-template-default #inner-content').height() - 40);
	// 	}
	// });
	
	// Change blog category to dropdown
	// if ($(window).width() < 640) {
		$('#categories-3 .widgettitle').append('<div class="arrow-down"></div>');
		$('#categories-3 .arrow-down').css({
			"float": "right", 
			"margin": "9px", 
			"width": "0", 
			"height": "0", 
			"border-left": "10px solid transparent", 
			"border-right": "10px solid transparent", 
			"border-top": "10px solid #555",
		});
		$('#categories-3 ul').hide();
		$('#categories-3 h4').css("cursor", "pointer");
		$('#categories-3 h4').click(function() {
			var dropdown = $(this).next();
			dropdown.slideToggle(function() {
				if (dropdown.is(':visible')) {
					$('#categories-3 .arrow-down').remove();
					$('#categories-3 .widgettitle').append('<div class="arrow-up"></div>');
					$('#categories-3 .arrow-up').css({
						"float": "right", 
						"margin": "9px", 
						"width": "0", 
						"height": "0", 
						"border-left": "10px solid transparent", 
						"border-right": "10px solid transparent", 
						"border-bottom": "10px solid #555",
					});
				} else {
					$('#categories-3 .arrow-up').remove();
					$('#categories-3 .widgettitle').append('<div class="arrow-down"></div>');
					$('#categories-3 .arrow-down').css({
						"float": "right", 
						"margin": "9px", 
						"width": "0", 
						"height": "0", 
						"border-left": "10px solid transparent", 
						"border-right": "10px solid transparent", 
						"border-top": "10px solid #555",
					});
				}
			});
		});
	// } else {
	// 	$('categories-3 ul').show();
	// }

	// Change search filter to dropdown
	$('.search-checkbox-container').hide();
	$('.search-property.trends .search-checkbox-container').show();
	$('.search-checkbox-label').css("cursor", "pointer");
	$('.search-checkbox-label').click(function() {
		$(this).next().slideToggle();
	});

	// Clone parent link for mobile menu
	$('#off-canvas .is-accordion-submenu-parent > a').each(function(){
	    var sub = $(this).next('ul');
	    $(this).clone().prependTo(sub).wrap('<li class="show-for-small-only"></li>');
	});

});