
					
	<section class="entry-content" itemprop="articleBody">
		<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('large'); ?></a>
		<div class="frame-properties">
		
		<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
		<div class="frame-name-or-style"><?php the_field('frame_name_or_style'); ?></div>
		<?php
			get_single_tax_terms($post, 'suggested_retail_price');
			get_single_tax_terms($post, 'product');
			get_single_tax_terms($post, 'face_shape');
		?>
		</div>
<!-- 		<?php the_content('<button class="tiny">' . __( 'Read more...', 'jointswp' ) . '</button>'); ?> -->
	</section> <!-- end article section -->
									    						