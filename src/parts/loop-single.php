<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						
	<header class="article-header">	
		<h1>EYEWEAR FASHION BLOG</h1>
		<p class="byline">Posted on <?php the_time('F j, Y') ?></p>
		<p><a href="/eyewear-fashion-blog">GO BACK</a></p>	
		<h3 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h3>
    </header> <!-- end article header -->
					
    <section class="entry-content" itemprop="articleBody">
		<?php the_content(); ?>
	</section> <!-- end article section -->
						
	<footer class="article-footer">
		<p class="tags"><?php the_tags('<span class="tags-title">' . __( 'Tags:', 'jointswp' ) . '</span> ', ', ', ''); ?></p>	
	</footer> <!-- end article footer -->
													
</article> <!-- end article -->