<div id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">					
	
<div class="blog-list-post-container">
	<header class="article-header">
		<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('full'); ?></a>
	</header> <!-- end article header -->
					
	<section class="entry-content" itemprop="articleBody">
		<p class="byline"><?php the_time('F j, Y'); ?></p>
		<h3><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
			<?php the_excerpt(); ?>
	</section> <!-- end article section -->
</div>
									    						
</div> <!-- end article -->