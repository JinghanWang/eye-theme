<!-- By default, this menu will use off-canvas for small
	 and a topbar for medium-up -->

<div class="row top-bar-navigation">
	<div class="large-12 columns">
		<div class="top-bar" id="top-bar-menu">
			<div class="top-bar-left float-left">
				<ul class="menu">
					<li class="menu-text"><a href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a></li>
				</ul>
			</div>
			<div class="top-bar-right show-for-large">
				<?php joints_top_nav(); ?>	
			</div>
			<div class="top-bar-right float-right hide-for-large">
				<ul class="menu">
					<li><button class="menu-icon" type="button" data-toggle="off-canvas"></button></li>
					
				</ul>
			</div>
		</div>
	</div>
</div>