<div class="breadcrumbs" typeof="BreadcrumbList">
    <?php if(function_exists('bcn_display'))
    {
        bcn_display();
    }?>
</div>

<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
						
	<header class="article-header">
		<h1 class="page-title"><?php the_title(); ?></h1>
	</header> <!-- end article header -->
					
    <section class="entry-content" itemprop="articleBody">
    	<?php the_post_thumbnail('large'); ?>
	    <?php the_content(); ?>
	    <?php wp_link_pages(); ?>
	</section> <!-- end article section -->
						
	<footer class="article-footer">
	</footer> <!-- end article footer -->

	<div class="content-footer-banner">
    	<a href="<?php the_field('footer_ad_link'); ?>"><?php get_banner_image('footer_ad', 'footer_ad_mobile'); ?></a>
    </div>
					
</article> <!-- end article -->