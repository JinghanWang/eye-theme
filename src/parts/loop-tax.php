<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">					
	
	<section class="entry-content" itemprop="articleBody">
		<p class="byline"><?php the_time('F j, Y'); ?></p>
		<h3><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
			<?php the_excerpt(); ?>
	</section> <!-- end article section -->
									    						
</article> <!-- end article -->