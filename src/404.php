<?php get_header(); ?>
			
	<div id="content">

		<div class="row">
			<div class="large-12 medium-12 columns">
				<div class="banner-image">
					<?php echo do_shortcode('[contentblock id=404-banner]'); ?>	
				</div>
			</div>
		</div>

		<div id="inner-content" class="row">
	
			<main id="main" class="large-12 medium-12 columns" role="main">

				<article id="content-not-found">
				
					<header class="article-header">
						<h1><?php _e( 'Epic 404 - Article Not Found', 'jointswp' ); ?></h1>
					</header> <!-- end article header -->
			
					<section class="entry-content">
						<h3>Sorry, the page you requested was not found.</h3>
						<p><?php _e( 'Please check the URL for proper spelling and capitalization. Also, you may try to find what you are looking for by searching in the field below.', 'jointswp' ); ?></p>
					</section> <!-- end article section -->

					<section class="search">
					    <p><?php get_search_form(); ?></p>
					</section> <!-- end search section -->
			
				</article> <!-- end article -->
	
			</main> <!-- end #main -->

		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>