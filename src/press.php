<?php 
/**
*	Template Name: Press Page
*/

get_header(); ?>
			
<div id="content">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<div class="row">
		<div class="large-12 medium-12 columns">
			<div class="banner-image">
				<?php get_banner_image('header_banner_image', 'header_banner_image_mobile'); ?>
			</div>
		</div>
	</div>

	<div id="inner-content" class="row">	
		    <main id="main" class="large-9 medium-8 columns" role="main">
			    <div class="breadcrumbs" typeof="BreadcrumbList">
				    <?php if(function_exists('bcn_display'))
				    {
				        bcn_display();
				    }?>
				</div>
				<div class="page-content">
					<header class="article-header">
						<h1 class="page-title"><?php the_title(); ?></h1>
					</header> <!-- end article header -->
				    <section class="entry-content" itemprop="articleBody">
				    	<?php the_post_thumbnail('large'); ?>
					    <?php the_content(); ?>
					</section> <!-- end article section -->
				</div>								

	<?php endwhile; endif; ?>							
			    		
				<?php
				$args = array(
						'post_type' => 'post',
						'posts_per_page' => 10,
						'paged' => $paged,
						'category_name' => 'press',
					);
				$wp_query = new WP_Query($args);

			    if ($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post(); ?>	
					<?php get_template_part( 'parts/loop', 'blog' ); ?>
				<?php endwhile; ?>
					<?php joints_page_navi(); ?>
				<?php else : ?>						
					<?php get_template_part( 'parts/content', 'missing' ); ?>	
				<?php endif; ?>
				<?php wp_reset_postdata(); ?>
																					
		    </main> <!-- end #main -->
		    
		    <?php get_sidebar(); ?>

		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer();

?>