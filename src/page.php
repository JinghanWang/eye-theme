<?php get_header(); ?>
	
	<div id="content">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	
		<div class="row">
			<div class="large-12 medium-12 columns">
				<div class="banner-image">
					<?php get_banner_image('header_banner_image', 'header_banner_image_mobile'); ?>	
				</div>
			</div>
		</div>
		
		<div id="inner-content" class="row">	
		    <main id="main" class="large-9 medium-8 columns" role="main">

			    	<?php get_template_part( 'parts/loop', 'page' ); ?>
			    
			    <?php endwhile; endif; ?>							
			    			
			</main> <!-- end #main -->
			
		    <?php get_sidebar(); ?>
		</div>
		    
		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>