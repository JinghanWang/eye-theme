<?php 
/**
*	Template Name: Single Gender Trend Page
*/

get_header(); ?>
	
	<div id="content">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?> 
		
		<div id="inner-content" class="row">

			<div class="banner-image large-12 medium-12 columns">
				<?php get_banner_image('header_banner_image', 'header_banner_image_mobile'); ?>
			</div>

		    <main id="main" class="large-9 medium-8 columns" role="main">

		    	<div class="breadcrumbs" typeof="BreadcrumbList">
				    <?php if(function_exists('bcn_display'))
				    {
				        bcn_display();
				    }?>
				</div>

				<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
										
					<header class="article-header">
						<h1 class="page-title"><?php the_title(); ?></h1>
					</header> <!-- end article header -->
									
				    <section class="entry-content" itemprop="articleBody">
					    <?php the_content(); ?>
					    <?php wp_link_pages(); ?>
					</section> <!-- end article section -->
										
					<footer class="article-footer">
						<?php 
						$trend = get_field('trend')->slug;
						$gender = get_field('gender')->slug;
						$args = array(
								'post_type' => 'frame',
								'posts_per_page' => -1,
								'tax_query' => array(
										array(
											'taxonomy' => 'gender',
											'field' => 'slug',
											'terms' => $gender,
										),
										array(
											'taxonomy' => 'trends',
											'field' => 'slug',
											'terms' => $trend,
										),
									),
							);
						$frames = new WP_Query($args);
						echo '<div class="single-frame-container">';
						while ($frames->have_posts()) : $frames->the_post();

							echo '<div class="single-frame"><a href="' . get_permalink() . '"><div class="frame-image">';
							the_post_thumbnail('medium');
							echo '</div></a><div class="frame-text"><div class="slide-tilte">';
							echo '<a href="' . get_permalink() . '">' . get_the_title() . '</a>';
							echo '</div><div class="frame-properties">';
							echo '<div class="frame-name-or-style">Style: ';
							the_field('frame_name_or_style');
							echo '</div>';
							get_single_tax_terms($post, 'product');
							echo '</div></div></div>';

						endwhile; wp_reset_postdata();
						echo '</div>';

						?>
					</footer> <!-- end article footer -->
									
				</article> <!-- end article -->
			    
			<?php endwhile; endif; ?>				
			    					
			</main> <!-- end #main -->

		    <?php get_sidebar(); ?>
		    
		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>